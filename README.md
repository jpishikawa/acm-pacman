# RHACM-pacman

## これは何？
RHACM（Red Hat Advanced Cluster Management for Kubernetes）を使って複数クラスタ上にアプリケーション（pacman）をデプロイするデモです。

## 準備
* ACMのHub Cluster用のOpenShiftクラスタを構築しておく
* 上記のクラスタにOperatorでACM、OpenShift GitOpsをインストールしておく
* Hub ClusterからデプロイするManaged Cluster用の各種情報（AWSの認証情報、SSHキー、等）を準備し、デプロイしておく
* xKSをHub Clusterにインポートする場合はあらかじめ構築しておく

## 手順
1. Hub Clusterに対しocコマンドを実行可能な作業環境に本リポジトリをクローン
2. `oc apply -f ./cr/*`
3. ACMコンソールのClusters -> Cluster Setを選択し、2でデプロイしたCluster Setにあらかじめ準備したManaged Clusterを登録する
4. ACMコンソールのApplicationsからArgo CDのApplicationSetを作成。参照するGitリポジトリとして本リポジトリを指定し、Pathに/pacmanを入力
5. ApplicationSetが作成されるとアプリがデプロイされます
